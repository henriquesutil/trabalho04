<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //$this->call(UsersTableSeeder::class);
        $this->call(InsereBairro::class);
        $this->call(InsereCategoria::class);
        $this->call(InsereDonoImoveis::class);
        $this->call(InsereImoveis::class);
    }
}
