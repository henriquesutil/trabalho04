<?php

use Illuminate\Database\Seeder;

class InsereCategoria extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categorias')->insert([
            'descricaocategoria'=>'Casa'
        ]);

        DB::table('categorias')->insert([
            'descricaocategoria'=>'Sobrado'
        ]);

        DB::table('categorias')->insert([
            'descricaocategoria'=>'Chácara'
        ]);

        DB::table('categorias')->insert([
            'descricaocategoria'=>'Apartamento'
        ]);

        DB::table('categorias')->insert([
            'descricaocategoria'=>'Sala Comercial'
        ]);
    }
}
