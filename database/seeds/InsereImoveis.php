<?php

use Illuminate\Database\Seeder;

class InsereImoveis extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('imoveis')->insert([
            'codbairro'=>1,
            'codcategoria'=>1,
            'coddonoimovel'=>1
        ]);

        DB::table('imoveis')->insert([
            'codbairro'=>1,
            'codcategoria'=>1,
            'coddonoimovel'=>5
        ]);

        DB::table('imoveis')->insert([
            'codbairro'=>5,
            'codcategoria'=>3,
            'coddonoimovel'=>3
        ]);

        DB::table('imoveis')->insert([
            'codbairro'=>3,
            'codcategoria'=>2,
            'coddonoimovel'=>4
        ]);

        DB::table('imoveis')->insert([
            'codbairro'=>2,
            'codcategoria'=>5,
            'coddonoimovel'=>2
        ]);
    }
}
