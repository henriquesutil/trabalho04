<?php

use Illuminate\Database\Seeder;
use App\Bairro;

class InsereBairro extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('bairros')->insert([
            'descricaobairro'=>'CENTRO'
        ]);

        DB::table('bairros')->insert([
            'descricaobairro'=>'LOTEAMENTO MORADA DO SOL'
        ]);

        DB::table('bairros')->insert([
            'descricaobairro'=>'PARQUE VERDE'
        ]);

        DB::table('bairros')->insert([
            'descricaobairro'=>'CARMIRANDA'
        ]);

        DB::table('bairros')->insert([
            'descricaobairro'=>'BERTINO WARMILING'
        ]);
    }
}
