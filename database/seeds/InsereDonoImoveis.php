<?php

use Illuminate\Database\Seeder;
use App\DonoImovel;

class InsereDonoImoveis extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('donoimoveis')->insert([
            'nomedonoimovel'=>'Pedro Henrique'
        ]);

        DB::table('donoimoveis')->insert([
            'nomedonoimovel'=>'Amanda'
        ]);

        DB::table('donoimoveis')->insert([
            'nomedonoimovel'=>'Nilse'
        ]);

        DB::table('donoimoveis')->insert([
            'nomedonoimovel'=>'Vanessa'
        ]);

        DB::table('donoimoveis')->insert([
            'nomedonoimovel'=>'Luiz'
        ]);
    }
}
