<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Imoveis;

class MigrationImoveis extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('imoveis', function (Blueprint $tabela){
            $tabela->increments('idimoveis');
            $tabela->integer('codbairro')->unsigned();
            $tabela->foreign('codbairro')->references('idbairro')->on('bairros');
            $tabela->integer('codcategoria')->unsigned();
            $tabela->foreign('codcategoria')->references('idcategoria')->on('categorias');
            $tabela->integer('coddonoimovel')->unsigned();
            $tabela->foreign('coddonoimovel')->references('iddonoimovel')->on('donoimoveis');
            $tabela->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('imoveis');
    }
}