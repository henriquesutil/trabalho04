<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DonoImovel extends Model
{
    protected $fillable = ['nomedonoimovel'];
}
