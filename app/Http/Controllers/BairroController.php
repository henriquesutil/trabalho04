<?php

namespace App\Http\Controllers;

use App\Bairro;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BairroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return DB::select('select * from bairros');
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::insert('insert into bairros (descricaobairro) values (?)', [$request->input('descricaobairro')]);
        return("Bairro cadastrado com sucesso!");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Bairro  $bairro
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return DB::select('select * from bairros where idbairro=?',[$id]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Bairro  $bairro
     * @return \Illuminate\Http\Response
     */
    public function edit(Bairro $bairro)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Bairro  $bairro
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::update('update bairros set descricaobairro=? where idbairro=?',[$request->input('descricaobairro'),$id]);
        return ("Registro alterado com sucesso!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Bairro  $bairro
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::delete('delete from bairros where idbairro=?',[$id]);
        return('Registro excluso com sucesso!');
    }
}
