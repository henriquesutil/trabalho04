<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class ImoveisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return DB::select('select nomedonoimovel as Dono, descricaocategoria as Categoria, descricaobairro as Bairro from imoveis
        inner join bairros
        on imoveis.codbairro = bairros.idbairro
        inner join categorias
        on imoveis.codcategoria = categorias.idcategoria
        inner join donoimoveis
        on imoveis.coddonoimovel = donoimoveis.iddonoimovel');
        }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::insert('insert into imoveis (codbairro, codcategoria, coddonoimovel) values (?, ?, ?)', [$request->input('codbairro'), $request->input('codcategoria'), $request->input('coddonoimovel')]);
        return('Registro incluso com sucesso!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return DB::select('select nomedonoimovel as Dono, descricaocategoria as Categoria, descricaobairro as Bairro from imoveis
        inner join bairros
        on imoveis.codbairro = bairros.idbairro
        inner join categorias
        on imoveis.codcategoria = categorias.idcategoria
        inner join donoimoveis
        on imoveis.coddonoimovel = donoimoveis.iddonoimovel where idimoveis=?', [$id]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::update('update imoveis set codbairro=? where idimoveis=?',[$request->input('codbairro'), $id]);
        return('Registro alterado com sucesso!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::delete('delete from imoveis where idimoveis=?',[$id]);
        return('Registro excluso com sucesso!');
    }
}
