<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use App\DonoImovel;

class DonoImovelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return DB::select('select * from donoimoveis');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try{



            //        $this->validate(
            //            $request,
            //            [
            //                'cidade' => 'required|min:3|max:10',
            //                'id_estado' => 'required|numeric'
            //            ]
            //        );

            //Chamar validacao
            $validacao = $this->validar($request);

            $validacao->after(function ($validacao) {
                $validacao->errors()->add('nomedonoimovel', 'Mensagem de validacao1!');
                //            $validacao->errors()->add('campo2', 'Mensagem de validacao2!');
                //            $validacao->errors()->add('campo3', 'Mensagem de validacao2!');
            });

            if ($validacao->fails()) {
                return response()->json([
                    'mensagem' => 'Erro',
                    'erros' => $validacao->errors()
                ], 400);
            }

            $donoimovel = new DonoImovel();

            $donoimovel->fill( $request->all() );
            $donoimovel->save();

            //Verifica se cadastrou a cidade no banco
            if( $donoimovel ){
                return response()->json( [$donoimovel], 201 );
            }else{
                return response()->json( ["mensagem" => "Erro ao cadastrar cidade"], 400 );
            }

            return $donoimovel;
        }catch ( \Exception $e ){
            return response()->json( ["mensagem" => $e->getMessage()], 500 );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DonoImovel  $donoImovel
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return DB::select('select * from donoimoveis where iddonoimovel=?', [$id]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DonoImovel  $donoImovel
     * @return \Illuminate\Http\Response
     */
    public function edit(DonoImovel $donoImovel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DonoImovel  $donoImovel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::update('update donoimoveis set nomedonoimovel=? where iddonoimovel=?', [$request->input('nomedonoimovel'), $id]);
        return ('Registro alterado com sucesso!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DonoImovel  $donoImovel
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::delete('delete from donoimoveis where iddonoimovel=?',[$id]);
        return('Registro excluso com sucesso!');
    }
}
